module.exports = {
  preset: "jest-puppeteer",
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.js", "!src/**/*.test.js", "!src/index.js"],
  coverageReporters: ["json", "lcov", "text", "html"],
  setupFilesAfterEnv: ["jest-codemods"],
  transform: {
    "^.+\\.jsx?$": "babel-jest",
  },
};
