import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";

const Exercises = () => {
  const [exercise, setExercise] = useState([]);

  useEffect(() => {
    // Fetch workout data
    const fetchExercises = async () => {
      try {
        const token = localStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        const response = await axios.get("/api/exercise", config);
        setExercise(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchExercises();
  }, []);

  return (
    <div className="page-container m-5">
      <h2 className="mb-5">Exercise List</h2>
      <Table className="w-50" striped bordered hover>
        <thead>
          <tr>
            <th>Exercise</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {exercise.map((exercise, index) => (
            <tr key={index}>
              <td>{exercise.name}</td>
              <td>{exercise.description}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Exercises;
