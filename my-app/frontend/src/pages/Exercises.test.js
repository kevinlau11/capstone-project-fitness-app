// exercises.test.js
import React from "react";
import { render, screen, act } from "@testing-library/react";
import axios from "axios";
import Exercises from "./Exercises";

jest.mock("axios");

describe("Exercises", () => {
  test("renders exercise list", async () => {
    // Mock the axios.get() function to return sample data
    axios.get.mockResolvedValue({
      data: [
        { name: "Exercise 1", description: "Description 1" },
        { name: "Exercise 2", description: "Description 2" },
      ],
    });

    await act(async () => {
      render(<Exercises />);
    });

    // Check if the table headers are rendered
    expect(screen.getByText("Exercise")).toBeInTheDocument();
    expect(screen.getByText("Description")).toBeInTheDocument();

    // Check if the exercise data is rendered
    expect(screen.getByText("Exercise 1")).toBeInTheDocument();
    expect(screen.getByText("Description 1")).toBeInTheDocument();
    expect(screen.getByText("Exercise 2")).toBeInTheDocument();
    expect(screen.getByText("Description 2")).toBeInTheDocument();
  });
});
