import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import axios from "axios";
import { toast } from "react-toastify";
import Register from "./Register";

jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("Register", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should display a success toast on successful registration", async () => {
    axios.post.mockResolvedValueOnce({
      status: 201,
      data: { message: "Registration successful" },
    });

    render(<Register />);

    // Fill out the registration form
    fireEvent.change(screen.getByPlaceholderText("Email"), {
      target: { value: "test@example.com" },
    });

    fireEvent.change(screen.getByPlaceholderText("Password"), {
      target: { value: "password123" },
    });

    fireEvent.click(screen.getByText("Register"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith("/api/register", {
        email: "test@example.com",
        password: "password123",
      });
      expect(toast.success).toHaveBeenCalledWith("Registration successful");
    });
  });

  it("should display an error toast on incorrect email or username", async () => {
    axios.post.mockRejectedValueOnce({
      status: 401,
      response: { data: { message: "Invalid email or password" } },
    });

    const { getByPlaceholderText, getByText } = render(<Register />);

    fireEvent.change(getByPlaceholderText("Email"), {
      target: { value: "test@example.com" },
    });
    fireEvent.change(getByPlaceholderText("Password"), {
      target: { value: "password123" },
    });

    fireEvent.click(getByText("Register"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith("/api/register", {
        email: "test@example.com",
        password: "password123",
      });
      expect(toast.error).toHaveBeenCalledWith("Invalid email or password");
    });
  });
});
