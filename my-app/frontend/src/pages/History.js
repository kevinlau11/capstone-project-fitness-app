import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";

const History = ({ cardios, strengths }) => {
  const [date, setDate] = useState("");
  const dateInputRef = useRef(null);
  const [strength, setStrength] = useState([]);
  const [cardio, setCardio] = useState([]);

  useEffect(() => {
    // Fetch workout data
    const fetchHistory = async () => {
      try {
        const token = localStorage.getItem("token");
        const date = new Date().toISOString().split("T")[0];
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };
        if (cardios && cardio.length) {
          setCardio(cardios);
        } else {
          const response = await axios.get("/api/cardio", config);
          setCardio(response.data);
        }

        if (strengths && strengths.length) {
          setStrength(strengths);
        } else {
          const response2 = await axios.get("/api/strength", config);
          setStrength(response2.data);
        }
        setDate(date);
      } catch (error) {
        console.error(error);
      }
    };

    fetchHistory();
  }, []);

  const handleDate = async (e) => {
    console.log("");
    console.log("HANDLE DATE");
    console.log(e.target.value);

    setDate(e.target.value);

    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          date: e.target.value,
        },
      };
      const response = await axios.get("/api/cardio", config);
      const response2 = await axios.get("/api/strength", config);

      setCardio(response.data);
      setStrength(response2.data);
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }
  };
  function StrengthTable() {
    return (
      <div className="page-container">
        <Table
          className="w-50"
          striped
          bordered
          hover
          aria-label="strength-table"
        >
          <thead>
            <tr>
              <th>Exercise</th>
              <th>Sets</th>
              <th>Reps</th>
              <th>Weight</th>
            </tr>
          </thead>
          <tbody>
            {strength.map((workout, index) => (
              <tr key={index}>
                <td>{workout.exercise}</td>
                <td>{workout.sets}</td>
                <td>{workout.reps}</td>
                <td>{workout.weight}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }

  function CardioTable() {
    return (
      <div className="page-container">
        <Table
          className="w-50"
          striped
          bordered
          hover
          aria-label="cardio-table"
        >
          <thead>
            <tr>
              <th>Exercise</th>
              <th>Distance</th>
              <th>Duration</th>
            </tr>
          </thead>
          <tbody>
            {cardio.map((workout, index) => (
              <tr key={index}>
                <td>{workout.exercise}</td>
                <td>{workout.distance}</td>
                <td>{workout.duration}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
  function None() {
    return (
      <div className="page-container">
        <h5>No exercises logged for this date!</h5>;
      </div>
    );
  }

  function Cardio() {
    if (cardio.length !== 0) {
      return <CardioTable />;
    }
    return <None />;
  }

  function Strength() {
    if (strength.length !== 0) {
      return <StrengthTable />;
    }
    return <None />;
  }

  return (
    <div className="page-container m-5">
      <h2 className="mb-4">Workout History</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Date</InputGroup.Text>
        <Form.Control
          type="date"
          onChange={handleDate}
          ref={dateInputRef}
          name="dob"
          placeholder="Date"
        />
      </InputGroup>
      <h3 className="mt-4 mb-2">
        {new Date(new Date(date).setHours(24)).toLocaleDateString()} Logs
      </h3>
      <h4 className="mt-2 mb-2">Cardio</h4>
      <Cardio />
      <h4 className="mt-3 mb-3">Strength</h4>
      <Strength />
    </div>
  );
};

export default History;
