import React, { useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./ProfileManagement.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";

const ProfileManagement = () => {
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [height, setHeight] = useState("");
  const [weight, setWeight] = useState("");

  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [goals, setGoals] = useState([]);

  useEffect(() => {
    // Fetch the user's profile data from the server and populate the fields
    fetchUserProfile();
    fetchGoal();
  }, []);

  const fetchUserProfile = async () => {
    try {
      const response = await axios.get("/api/profile");
      const { age, gender, height, weight } = response.data;
      setAge(age);
      setGender(gender);
      setHeight(height);
      setWeight(weight);
    } catch (error) {
      console.error("Fetching Error:", error);
    }
  };

  const fetchGoal = async () => {
    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.get("/api/goal", config);
      const { name, value } = response.data[0];
      setName(name);
      setValue(value);
      const goals = [];
      goals.push("Weight Loss", "Weight Gain", "More Sleep", "Workout More");
      setGoals(goals);
    } catch (error) {
      console.error("Fetching Error:", error);
    }
  };

  const handleProfileUpdate = async () => {
    if (age === null || gender === null || height === null || weight === null) {
      toast.error("Inputs cannot be blank!");
      return;
    }
    if (isNaN(age) || isNaN(height) || isNaN(weight)) {
      toast.error("Age, height, and weight must be a number!");
      return;
    }
    try {
      const response = await axios.post("/api/profile", {
        age,
        gender,
        height,
        weight,
      });
      // Profile update successful, handle the success case
      console.log(response.data.message);
      toast.success(response.data.message);
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }
  };

  const handleGoal = async () => {
    if (name === null || value === null) {
      toast.error("Inputs cannot be blank!");
      return;
    }
    if (isNaN(value)) {
      toast.error("Value must be a number!");
      return;
    }

    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.post(
        "/api/goal",
        {
          name,
          value,
        },
        config
      );

      console.log(response.data.message);
      toast.success(response.data.message);
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }
  };

  return (
    <div className="page-container m-5">
      <h2 className="mb-4">Profile Management</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Age</InputGroup.Text>
        <Form.Control
          aria-label="age"
          aria-describedby="age"
          id="age"
          placeholder="Enter age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Gender</InputGroup.Text>
        <Form.Control
          aria-label="gender"
          aria-describedby="gender"
          id="gender"
          placeholder="Enter gender"
          value={gender}
          onChange={(e) => setGender(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Height</InputGroup.Text>
        <Form.Control
          aria-label="height"
          aria-describedby="height"
          id="height"
          placeholder="Enter height in inches"
          value={height}
          onChange={(e) => setHeight(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Weight</InputGroup.Text>
        <Form.Control
          aria-label="weight"
          aria-describedby="weight"
          id="weight"
          placeholder="Enter weight in lbs"
          value={weight}
          onChange={(e) => setWeight(e.target.value)}
        />
      </InputGroup>
      <Button variant="primary" onClick={handleProfileUpdate} type="submit">
        Save Profile
      </Button>

      <h2 className="m-4">Enter Goal</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Goal</InputGroup.Text>
        <Form.Control
          aria-label="name"
          aria-describedby="name"
          id="name"
          placeholder="Enter goal"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <DropdownButton
          variant="outline-secondary"
          title=""
          id="input-group-dropdown-2"
          align="end"
        >
          {goals.map((goal, index) => (
            <Dropdown.Item onClick={(e) => setName(goal)}>{goal}</Dropdown.Item>
          ))}
        </DropdownButton>
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Value</InputGroup.Text>
        <Form.Control
          aria-label="value"
          aria-describedby="value"
          id="value"
          placeholder="Enter Value"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      </InputGroup>
      <Button variant="primary" onClick={handleGoal} type="submit">
        Save Goal
      </Button>
    </div>
  );
};

export default ProfileManagement;
