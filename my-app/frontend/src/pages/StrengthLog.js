import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";

const StrengthLog = () => {
  const [exercise, setExercise] = useState("");
  const [sets, setSets] = useState("");
  const [reps, setReps] = useState("");
  const [weight, setWeight] = useState("");
  const [date, setDate] = useState("");
  const dateInputRef = useRef(null);
  const [strength, setStrength] = useState([]);

  const [exerciseList, setExerciseList] = useState([]);

  useEffect(() => {
    const fetchExercises = async () => {
      try {
        const token = localStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        const response = await axios.get("/api/exercise", config);
        setExerciseList(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchExercises();
  }, []);

  useEffect(() => {
    // Fetch workout data
    const fetchStrength = async () => {
      try {
        const token = localStorage.getItem("token");
        const date = new Date().toISOString().split("T")[0];
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/strength", config);
        setStrength(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchStrength();
  }, []);

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  const handleLog = async () => {
    if (
      exercise.trim() === "" ||
      sets.trim() === "" ||
      reps.trim() === "" ||
      weight.trim() === "" ||
      date === ""
    ) {
      toast.error("Fill in missing inputs!");
      return;
    }
    if (isNaN(sets) || isNaN(reps) || isNaN(weight)) {
      toast.error("Sets, reps, and weight must be a number!");
      return;
    }

    const token = localStorage.getItem("token");
    console.log(token);
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await axios.post(
        "/api/strength",
        {
          exercise,
          sets,
          reps,
          weight,
          date,
        },
        config
      );
      // Profile update successful, handle the success case
      console.log(response.data.message);
      toast.success(response.data.message);
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }

    try {
      const token = localStorage.getItem("token");
      console.log(token);
      const date = new Date().toISOString().split("T")[0];
      console.log("Today IS BELOW:");
      console.log(date);
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          date: date,
        },
      };

      const response = await axios.get("/api/strength", config);
      setStrength(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="page-container m-5">
      <h2 className="mb-5">Log Strength Exercise</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Exercise
        </InputGroup.Text>
        <Form.Control
          aria-label="exercise"
          aria-describedby="exercise"
          id="exercise"
          placeholder="Enter exercise name"
          value={exercise}
          onChange={(e) => setExercise(e.target.value)}
        />
        <DropdownButton
          variant="outline-secondary"
          title=""
          id="input-group-dropdown-2"
          align="end"
        >
          {exerciseList.map((exerciseList, index) => (
            <Dropdown.Item onClick={(e) => setExercise(exerciseList.name)}>
              {exerciseList.name}
            </Dropdown.Item>
          ))}
        </DropdownButton>
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Sets</InputGroup.Text>
        <Form.Control
          aria-label="sets"
          aria-describedby="sets"
          id="sets"
          placeholder="Enter number of sets"
          value={sets}
          onChange={(e) => setSets(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Reps</InputGroup.Text>
        <Form.Control
          aria-label="reps"
          aria-describedby="reps"
          id="reps"
          placeholder="Enter number of reps"
          value={reps}
          onChange={(e) => setReps(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Weight</InputGroup.Text>
        <Form.Control
          aria-label="weight"
          aria-describedby="weight"
          placeholder="Enter how much weight was used"
          id="weight"
          value={weight}
          onChange={(e) => setWeight(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Date</InputGroup.Text>
        <Form.Control
          type="date"
          aria-label="date"
          aria-describedby="date"
          id="date"
          onChange={handleDate}
          ref={dateInputRef}
          placeholder="Date"
        />
      </InputGroup>
      <Button variant="primary" onClick={handleLog} type="submit">
        Log Exercise
      </Button>

      <h2 className="mt-3 mb-3">Todays Logs</h2>

      <Table className="w-50" striped bordered hover>
        <thead>
          <tr>
            <th>Exercise</th>
            <th>Sets</th>
            <th>Reps</th>
            <th>Weight</th>
          </tr>
        </thead>
        <tbody>
          {strength.map((workout, index) => (
            <tr key={index}>
              <td>{workout.exercise}</td>
              <td>{workout.sets}</td>
              <td>{workout.reps}</td>
              <td>{workout.weight}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default StrengthLog;
