import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";
import axios from "axios";
import Sleep from "./Sleep";
import { toast } from "react-toastify";

// Mock axios
jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("Sleep component", () => {
  test("renders the component", () => {
    render(<Sleep />);

    // Check if the component renders without errors
    expect(screen.getByText("Track Sleep and Weight")).toBeInTheDocument();
  });

  test("handles form submission", async () => {
    // Mock axios post request
    axios.post.mockResolvedValue({
      data: { message: "Log Successful" },
    });
    render(<Sleep />);

    // Fill in the form inputs
    fireEvent.change(screen.getByLabelText("sleep"), {
      target: { value: 2 },
    });
    fireEvent.change(screen.getByLabelText("weight"), {
      target: { value: "100" },
    });
    fireEvent.change(screen.getByLabelText("date"), {
      target: { value: "2023-06-25" },
    });

    // Click on the submit button
    // fireEvent.click(screen.getByText("Log Sleep and Weight"));
    const token = "mockToken";
    localStorage.setItem("token", token);

    await act(async () => {
      // Submit the form
      fireEvent.click(screen.getByText("Log Sleep and Weight"));

      // Wait for the state updates to be applied
      await waitFor(() => {
        expect(axios.post).toHaveBeenCalledWith(
          "/api/sleep",
          {
            length: "2",
            weight: "100",
            date: "2023-06-25",
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
      });

      // Check that success toast message is displayed
      expect(toast.success).toHaveBeenCalledWith("Log Successful");
    });
  });
});
