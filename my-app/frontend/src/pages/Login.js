import React, { useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./Register.css";
import { useNavigate } from "react-router-dom";

const Login = ({ onLogin }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    localStorage.removeItem("token");
  }, []);

  const handleLogin = async () => {
    if (email.trim() === "") {
      toast.error("Email is required");
      return;
    }
    if (password.trim() === "") {
      toast.error("Password is required");
      return;
    }
    try {
      const response = await axios.post("/api/login", {
        email,
        password,
      });
      toast.success(response.data.message);
      console.log(response.data);
      onLogin();
      const token = response.data.token;
      console.log("token is below:");
      console.log(token);

      localStorage.setItem("token", token);

      navigate("/profile");
    } catch (error) {
      // Handle login error
      localStorage.removeItem("token");
      toast.error(error.response.data.message);
      console.error("Login failed:", error.message);
    }
  };

  return (
    <div className="mt-5">
      <h2>Login Form</h2>
      <input
        className="input"
        type="email"
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        required
      />
      <input
        className="input"
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        required
      />
      <button className="button" onClick={handleLogin} type="submit">
        Login
      </button>
    </div>
  );
};

export default Login;
