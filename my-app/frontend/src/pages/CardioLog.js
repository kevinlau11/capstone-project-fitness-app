import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";

const CardioLog = () => {
  const [exercise, setExercise] = useState("");
  const [distance, setDistance] = useState("");
  const [duration, setDuration] = useState("");
  const [date, setDate] = useState("");
  const dateInputRef = useRef(null);

  // const [workout, setWorkout] = useState("");

  const [cardio, setCardio] = useState([]);

  useEffect(() => {
    // Fetch workout data
    const fetchCardio = async () => {
      try {
        const token = localStorage.getItem("token");
        console.log(token);
        const date = new Date().toISOString().split("T")[0];
        console.log("Today IS BELOW:");
        console.log(date);
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/cardio", config);
        setCardio(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchCardio();
  }, []);

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  const handleLog = async () => {
    if (
      exercise.trim() === "" ||
      distance.trim() === "" ||
      duration.trim() === "" ||
      date === ""
    ) {
      toast.error("Fill in missing inputs!");
      return;
    }
    if (isNaN(distance) || isNaN(duration)) {
      toast.error("Distance and duration must be a number!");
      return;
    }
    const token = localStorage.getItem("token");
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    console.log("DATE IS BELOW:");
    console.log(date);
    try {
      const response = await axios.post(
        "/api/cardio",
        {
          exercise,
          distance,
          duration,
          date,
        },
        config
      );
      // Profile update successful, handle the success case
      console.log(response.data.message);
      toast.success(response.data.message);

      try {
        const token = localStorage.getItem("token");
        console.log(token);
        const date = new Date().toISOString().split("T")[0];
        console.log("Today IS BELOW:");
        console.log(date);
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/cardio", config);
        setCardio(response.data);
      } catch (error) {
        console.error(error);
      }
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }
  };

  return (
    <div className="page-container m-5">
      <h2 className="mb-5">Log Cardio Exercise</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Exercise
        </InputGroup.Text>
        <Form.Control
          aria-label="exercise"
          aria-describedby="exercise"
          id="exercise"
          placeholder="Enter exercise name"
          value={exercise}
          onChange={(e) => setExercise(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Distance
        </InputGroup.Text>
        <Form.Control
          aria-label="distance"
          aria-describedby="distance"
          id="distance"
          placeholder="Enter the distance in miles"
          value={distance}
          onChange={(e) => setDistance(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Duration
        </InputGroup.Text>
        <Form.Control
          aria-label="duration"
          aria-describedby="duration"
          id="duration"
          placeholder="Enter duration of exercise"
          value={duration}
          onChange={(e) => setDuration(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25" controlId="dob">
        <InputGroup.Text>Date</InputGroup.Text>
        <Form.Control
          type="date"
          aria-label="date"
          aria-describedby="date"
          id="date"
          onChange={handleDate}
          ref={dateInputRef}
          placeholder="Date"
        />
      </InputGroup>
      <Button variant="primary" onClick={handleLog} type="submit">
        Log Exercise
      </Button>

      <h2 className="mt-3 mb-3">Todays Logs</h2>
      <Table className="w-50" striped bordered hover>
        <thead>
          <tr>
            <th>Exercise</th>
            <th>Distance</th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody>
          {cardio.map((workout, index) => (
            <tr key={index}>
              <td>{workout.exercise}</td>
              <td>{workout.distance}</td>
              <td>{workout.duration}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default CardioLog;
