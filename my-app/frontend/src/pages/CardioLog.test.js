import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import axios from "axios";
import CardioLog from "./CardioLog";

// Mock axios
jest.mock("axios");
describe("CardioLog", () => {
  beforeEach(() => {
    // Clear all mocks before each test
    jest.clearAllMocks();
  });

  test("logs a cardio exercise", async () => {
    const token = "mockToken";
    localStorage.setItem("token", token);
    render(<CardioLog />);

    const mockResponse = {
      data: {
        message: "Exercise logged successfully",
      },
    };
    axios.post.mockResolvedValue(mockResponse);

    // Fill in the form inputs
    const exerciseInput = screen.getByLabelText("exercise");
    const distanceInput = screen.getByLabelText("distance");
    const durationInput = screen.getByLabelText("duration");
    const dateInput = screen.getByLabelText("date");

    fireEvent.change(exerciseInput, { target: { value: "Running" } });
    fireEvent.change(distanceInput, { target: { value: "5" } });
    fireEvent.change(durationInput, { target: { value: "30" } });
    fireEvent.change(dateInput, { target: { value: "2023-07-01" } });

    // Click the "Log Exercise" button
    const logButton = screen.getByText("Log Exercise");
    fireEvent.click(logButton);

    // Assert that the axios post request was called with the correct data
    expect(axios.post).toHaveBeenCalledWith(
      "/api/cardio",
      {
        exercise: "Running",
        distance: "5",
        duration: "30",
        date: "2023-07-01",
      },
      expect.any(Object)
    );
  });
});
