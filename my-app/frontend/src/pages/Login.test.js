import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import axios from "axios";
import { toast } from "react-toastify";
import Login from "./Login";

jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => jest.fn(),
}));

describe("Login", () => {
  it("should call onLogin and display success toast on successful login", async () => {
    const onLoginMock = jest.fn();
    const response = {
      status: 200,
      data: {
        message: "Login successful",
        token: "test-token",
      },
    };
    axios.post.mockResolvedValueOnce(response);
    const mockOnLogin = jest.fn();

    render(<Login onLogin={mockOnLogin} />);

    fireEvent.change(screen.getByPlaceholderText("Email"), {
      target: { value: "test@example.com" },
    });

    fireEvent.change(screen.getByPlaceholderText("Password"), {
      target: { value: "password123" },
    });

    fireEvent.click(screen.getByText("Login"));
    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith("/api/login", {
        email: "test@example.com",
        password: "password123",
      });
      // expect(screen.getByText(response.data.message)).toBeInTheDocument();
      expect(mockOnLogin).toHaveBeenCalled();
      expect(toast.success).toHaveBeenCalledWith("Login successful");
      expect(localStorage.getItem("token")).toBe(response.data.token);
    });
  });

  it("should display error toast on failed login", async () => {
    const errorResponse = {
      response: {
        status: 401,
        data: {
          message: "Login failed",
        },
      },
    };
    axios.post.mockRejectedValueOnce(errorResponse);
    render(<Login />);

    // Fill out the login form
    fireEvent.change(screen.getByPlaceholderText("Email"), {
      target: { value: "test@example.com" },
    });

    fireEvent.change(screen.getByPlaceholderText("Password"), {
      target: { value: "password123" },
    });
    fireEvent.click(screen.getByText("Login"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith("/api/login", {
        email: "test@example.com",
        password: "password123",
      });
      // expect(
      //   screen.getByText(errorResponse.response.data.message)
      // ).toBeInTheDocument();
      expect(toast.error).toHaveBeenCalledWith("Login failed");

      expect(localStorage.getItem("token")).toBeNull();
    });
  });
});
