import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";

const Nutrition = () => {
  const [calories, setCalories] = useState("");
  const [carbohydrates, setCarbohydrates] = useState("");
  const [fat, setFat] = useState("");
  const [protein, setProtein] = useState("");
  const [sugar, setSugar] = useState("");
  const [cholesterol, setCholesterol] = useState("");
  const [sodium, setSodium] = useState("");
  const [date, setDate] = useState("");
  const dateInputRef = useRef(null);
  const [nutrition, setNutrition] = useState([]);

  useEffect(() => {
    // Fetch workout data
    const fetchNutrition = async () => {
      try {
        const token = localStorage.getItem("token");
        console.log(token);
        const date = new Date().toISOString().split("T")[0];
        console.log("Today IS BELOW:");
        console.log(date);
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/nutrition", config);
        setNutrition(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchNutrition();
  }, []);

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  const handleLog = async () => {
    if (
      // exercise.trim() === "" ||
      // sets.trim() === "" ||
      // reps.trim() === "" ||
      // weight.trim() === "" ||  
      date === ""
    ) {
      toast.error("Date cannot be blank!");
      return;
    }
    // if (isNaN(sets) || isNaN(reps) || isNaN(weight)) {
    //   toast.error("Sets, reps, and weight must be a number!");
    //   return;
    // }

    const token = localStorage.getItem("token");
    console.log(token);
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await axios.post(
        "/api/nutrition",
        {
          calories,
          carbohydrates,
          fat,
          protein,
          sugar,
          cholesterol,
          sodium,
          date,
        },
        config
      );
      // Profile update successful, handle the success case
      setCalories("");
      console.log(response.data.message);
      toast.success(response.data.message);
    } catch (error) {
      console.error("Error:", error);
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }

    try {
      const token = localStorage.getItem("token");
      console.log(token);
      const date = new Date().toISOString().split("T")[0];
      console.log("Today IS BELOW:");
      console.log(date);
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          date: date,
        },
      };

      const response = await axios.get("/api/nutrition", config);
      setNutrition(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="page-container m-5">
      <h2 className="mb-5">Nutrition Tracker</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Calories
        </InputGroup.Text>
        <Form.Control
          aria-label="calories"
          aria-describedby="calories"
          id="calories"
          placeholder="Enter number of calories"
          value={calories}
          onChange={(e) => setCalories(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Carbohydrates</InputGroup.Text>
        <Form.Control
          aria-label="carbohydrates"
          aria-describedby="carbohydrates"
          id="carbohydrates"
          placeholder="Enter carbohydrates in grams"
          value={carbohydrates}
          onChange={(e) => setCarbohydrates(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Fat</InputGroup.Text>
        <Form.Control
          aria-label="fat"
          aria-describedby="fat"
          id="fat"
          placeholder="Enter fat in grams"
          value={fat}
          onChange={(e) => setFat(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Protein
        </InputGroup.Text>
        <Form.Control
          aria-label="protein"
          aria-describedby="protein"
          id="protein"
          placeholder="Enter protein in grams"
          value={protein}
          onChange={(e) => setProtein(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Sugar</InputGroup.Text>
        <Form.Control
          aria-label="sugar"
          aria-describedby="sugar"
          id="sugar"
          placeholder="Enter sugar in grams"
          value={sugar}
          onChange={(e) => setSugar(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">
          Cholesterol
        </InputGroup.Text>
        <Form.Control
          aria-label="cholesterol"
          aria-describedby="cholesterol"
          id="cholesterol"
          placeholder="Enter cholesterol in grams"
          value={cholesterol}
          onChange={(e) => setCholesterol(e.target.value)}
        />
      </InputGroup>{" "}
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Sodium</InputGroup.Text>
        <Form.Control
          aria-label="sodium"
          aria-describedby="sodium"
          id="sodium"
          placeholder="Enter sodium in mg"
          value={sodium}
          onChange={(e) => setSodium(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Date</InputGroup.Text>
        <Form.Control
          type="date"
          aria-label="date"
          aria-describedby="date"
          id="date"
          onChange={handleDate}
          ref={dateInputRef}
          placeholder="Date"
        />
      </InputGroup>
      <Button variant="primary" onClick={handleLog} type="submit">
        Log Nutrition Intake
      </Button>
      <h2 className="mt-3 mb-3">Todays Logs</h2>
      <Table className="w-50" striped bordered hover>
        <thead>
          <tr>
            <th>Calories</th>
            <th>Carbohydrates</th>
            <th>Fat</th>
            <th>Protein</th>
            <th>Sugar</th>
            <th>Cholesterol</th>
            <th>Sodium</th>
          </tr>
        </thead>
        <tbody>
          {nutrition.map((nutrition, index) => (
            <tr key={index}>
              <td>{nutrition.calories}</td>
              <td>{nutrition.carbohydrates}</td>
              <td>{nutrition.fat}</td>
              <td>{nutrition.protein}</td>
              <td>{nutrition.sugar}</td>
              <td>{nutrition.cholesterol}</td>
              <td>{nutrition.sodium}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Nutrition;
