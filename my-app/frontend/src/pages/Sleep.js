import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Table from "react-bootstrap/Table";

const Sleep = () => {
  const [length, setLength] = useState("");
  const [weight, setWeight] = useState("");
  const [date, setDate] = useState("");
  const dateInputRef = useRef(null);
  const [sleepList, setSleepList] = useState([]);

  useEffect(() => {
    // Fetch workout data
    const fetchSleep = async () => {
      try {
        const token = localStorage.getItem("token");
        const date = new Date().toISOString().split("T")[0];
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/sleep", config);
        setSleepList(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchSleep();
  }, []);

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  const handleLog = async () => {
    if (weight.trim() === "" || date === "") {
      toast.error("Fill in missing inputs!");
      return;
    }
    // if (isNaN(sets) || isNaN(reps) || isNaN(weight)) {
    //   toast.error("Sets, reps, and weight must be a number!");
    //   return;
    // }

    const token = localStorage.getItem("token");
    console.log(token);
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await axios.post(
        "/api/sleep",
        {
          length,
          weight,
          date,
        },
        config
      );
      setLength("");
      setWeight("");
      console.log(response.data.message);
      toast.success(response.data.message);
    } catch (error) {
      // console.error("Error:", error);
      // console.error(error.response.data);
      // toast.error(error.response.data.message);
      toast.error("User can only log one sleep and weight per day.");
    }

    try {
      const token = localStorage.getItem("token");
      console.log(token);
      const date = new Date().toISOString().split("T")[0];
      console.log("Today IS BELOW:");
      console.log(date);
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          date: date,
        },
      };

      const response = await axios.get("/api/sleep", config);
      setSleepList(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="page-container m-5">
      <h2 className="mb-5">Track Sleep and Weight</h2>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Sleep</InputGroup.Text>
        <Form.Control
          aria-label="sleep"
          aria-describedby="sleep"
          id="sleep"
          placeholder="Enter hours of sleep"
          value={length}
          onChange={(e) => setLength(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text id="inputGroup-sizing-default">Weight</InputGroup.Text>
        <Form.Control
          aria-label="weight"
          aria-describedby="weight"
          id="weight"
          placeholder="Enter weight for that day in lbs"
          value={weight}
          onChange={(e) => setWeight(e.target.value)}
        />
      </InputGroup>
      <InputGroup className="mb-3 w-25">
        <InputGroup.Text>Date</InputGroup.Text>
        <Form.Control
          type="date"
          aria-label="date"
          aria-describedby="date"
          id="date"
          onChange={handleDate}
          ref={dateInputRef}
          placeholder="Date"
        />
      </InputGroup>
      <Button variant="primary" onClick={handleLog} type="submit">
        Log Sleep and Weight
      </Button>

      <h2 className="mt-3 mb-3">Todays Logs</h2>

      <Table className="w-50" striped bordered hover>
        <thead>
          <tr>
            <th>Sleep</th>
            <th>Weight</th>
          </tr>
        </thead>
        <tbody>
          {sleepList.map((sleep, index) => (
            <tr key={index}>
              <td>{sleep.length}</td>
              <td>{sleep.weight}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Sleep;
