import React, { useState } from "react";
import { render, screen, act } from "@testing-library/react";
import axios from "axios";
import History from "./History";

jest.mock("axios");

describe("History component", () => {
  test("renders the history component and displays cardio and strength logs", async () => {
    const cardioData = [
      { exercise: "Running", distance: "5 km", duration: "30 minutes" },
      { exercise: "Cycling", distance: "10 km", duration: "45 minutes" },
    ];
    const strengthData = [
      { exercise: "Bench Press", sets: 3, reps: 10, weight: 100 },
      { exercise: "Squats", sets: 4, reps: 8, weight: 120 },
    ];
    axios.get.mockResolvedValueOnce({ data: cardioData });
    axios.get.mockResolvedValueOnce({ data: strengthData });

    await act(async () => {
      render(<History cardios={cardioData} strengths={strengthData} />);
    });
    const cardioTable = screen.getByRole("table", { name: "cardio-table" });
    const strengthTable = screen.getByRole("table", { name: "strength-table" });

    expect(cardioTable).toBeInTheDocument();
    expect(strengthTable).toBeInTheDocument();

    const cardioRows = screen.getAllByRole("row", { within: cardioTable });
    const strengthRows = screen.getAllByRole("row", { within: strengthTable });

    expect(cardioRows).toHaveLength(cardioData.length + 4);
    expect(strengthRows).toHaveLength(strengthData.length + 4);
  });
});
