import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";
import axios from "axios";
import Nutrition from "./Nutrition";
import { toast } from "react-toastify";

// Mock axios
jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("nutrition component", () => {
  test("renders the component", () => {
    render(<Nutrition />);

    // Check if the component renders without errors
    expect(screen.getByText("Nutrition Tracker")).toBeInTheDocument();
  });

  test("handles form submission", async () => {
    // Mock axios post request
    axios.post.mockResolvedValue({
      data: { message: "Log Successful" },
    });
    render(<Nutrition />);

    // Fill in the form inputs
    fireEvent.change(screen.getByLabelText("calories"), {
      target: { value: 2 },
    });
    fireEvent.change(screen.getByLabelText("date"), {
      target: { value: "2023-06-25" },
    });

    // Click on the submit button
    // fireEvent.click(screen.getByText("Log Sleep and Weight"));
    const token = "mockToken";
    localStorage.setItem("token", token);

    await act(async () => {
      // Submit the form
      fireEvent.click(screen.getByText("Log Nutrition Intake"));

      // Wait for the state updates to be applied
      await waitFor(() => {
        expect(axios.post).toHaveBeenCalledWith(
          "/api/nutrition",
          {
            calories: "2",
            carbohydrates: "",
            fat: "",
            protein: "",
            sugar: "",
            cholesterol: "",
            sodium: "",
            date: "2023-06-25",
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
      });

      // Check that success toast message is displayed
      expect(toast.success).toHaveBeenCalledWith("Log Successful");
    });
  });
});
