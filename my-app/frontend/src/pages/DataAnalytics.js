import React, { useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";

import Table from "react-bootstrap/Table";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
const DataAnalytics = () => {
  const [datas, setDatas] = useState({});
  const [dates, setDates] = useState([]);
  const [isTable, setIsTable] = useState(true);
  const [labels, setLabels] = useState([]);

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Bar Chart",
      },
    },
  };

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

  useEffect(() => {
    const fetchHistory = async () => {
      try {
        const token = localStorage.getItem("token");
        const date = new Date().toISOString().split("T")[0];
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
          params: {
            date: date,
          },
        };

        const response = await axios.get("/api/calcdata", config);
        setDatas(response.data);
        // console.log(dates);
        // setTemp(response.data.dataByDate);
        let temp = response.data.dataByDate;
        // setDates(Object.entries(dates));
        let x = [];
        let tempLabels = [];
        for (const [key, value] of Object.entries(temp)) {
          // console.log(`${key}: ${value.calories}`);
          tempLabels.push(key);
          x.push({
            date: key,
            calories: value.calories,
            sleep: value.sleep,
            distance: value.distance,
            duration: value.duration,
          });
        }
        tempLabels.push("Average");
        tempLabels.push("Total");
        setLabels(tempLabels);
        setDates(x);
        x.sort(
          (firstItem, secondItem) =>
            Date.parse(firstItem.date) - Date.parse(secondItem.date)
        );
        console.log(labels);
        console.log("LABELS ABOVE");
      } catch (error) {
        console.error(error);
      }
    };

    fetchHistory();
  }, []);

  const data = {
    labels,
    datasets: [
      {
        label: "Sleep",
        data: labels.map((element, index) => {
          if (element === "Average") {
            return datas.averageSleep;
          } else if (element === "Total") {
            return datas.totalSleep;
          }
          return dates[index].sleep;
        }),
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
      {
        label: "Distance",
        data: labels.map((element, index) => {
          if (element === "Average") {
            return datas.averageDistance;
          } else if (element === "Total") {
            return datas.totalDistance;
          }
          return dates[index].distance;
        }),
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "Duration",
        data: labels.map((element, index) => {
          if (element === "Average") {
            return datas.averageDuration;
          } else if (element === "Total") {
            return datas.totalDuration;
          }
          return dates[index].duration;
        }),
        backgroundColor: "rgba(255, 250, 132, 0.5)",
      },
    ],
  };

  const data2 = {
    labels,
    datasets: [
      {
        label: "Calories",
        data: labels.map((element, index) => {
          if (element === "Average") {
            return datas.averageCalories;
          } else if (element === "Total") {
            return datas.totalCalories;
          }
          return dates[index].calories;
        }),
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
    ],
  };

  const handleChange = (e) => {
    setIsTable(!isTable);
  };

  function View({ isTable }) {
    if (isTable) {
      return (
        <Table
          className="w-50"
          striped
          bordered
          hover
          aria-label="strength-table"
        >
          <thead>
            <tr>
              <th>Date</th>
              <th>Calories</th>
              <th>Sleep(hr)</th>
              <th>Distance(mi)</th>
              <th>Duration(hr)</th>
            </tr>
          </thead>
          <tbody>
            {dates.map((x, index) => (
              <tr key={index}>
                <td>{x.date}</td>
                <td>{x.calories}</td>
                <td>{x.sleep}</td>
                <td>{x.distance}</td>
                <td>{x.duration}</td>
              </tr>
            ))}
            <tr>
              <td>Total</td>
              <td>{datas.totalCalories}</td>
              <td>{datas.totalSleep}</td>
              <td>{datas.totalDistance}</td>
              <td>{datas.totalDuration}</td>
            </tr>
            <tr>
              <td>Average</td>
              <td>{datas.averageCalories}</td>
              <td>{datas.averageSleep}</td>
              <td>{datas.averageDistance}</td>
              <td>{datas.averageDuration}</td>
            </tr>
          </tbody>
        </Table>
      );
    }
    return (
      <>
        <Line
          className="bar"
          height="60px"
          width="200px"
          options={options}
          data={data2}
          data-testid="line-chart"
        />
        <Line
          className="bar"
          height="60px"
          width="200px"
          options={options}
          data={data}
          data-testid="line-chart2"
        />
      </>
    );
  }

  function None() {
    return (
      <div className="page-container">
        <h5>No exercises logged for this date!</h5>;
      </div>
    );
  }

  return (
    <div className="page-container m-5">
      <h2 className="mb-3">Data Analytics</h2>

      <h4 className="mt-3 mb-3">Past 7 days</h4>
      <View isTable={isTable} />
      <Button
        className="mb-3"
        variant="primary"
        onClick={handleChange}
        type="submit"
      >
        Toggle View
      </Button>
    </div>
  );
};

export default DataAnalytics;
