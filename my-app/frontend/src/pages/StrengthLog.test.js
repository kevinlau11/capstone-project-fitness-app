import React from "react";
import {
  render,
  screen,
  fireEvent,
  waitFor,
  act,
} from "@testing-library/react";
import axios from "axios";
import StrengthLog from "./StrengthLog";
import { toast } from "react-toastify";

jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("StrengthLog", () => {
  beforeEach(() => {
    render(<StrengthLog />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("logs an exercise successfully", async () => {
    axios.get.mockResolvedValue({ data: [] });
    axios.post.mockResolvedValue({
      data: { message: "Exercise logged successfully" },
    });

    // Fill in the form inputs
    fireEvent.change(screen.getByLabelText("exercise"), {
      target: { value: "Bench Press" },
    });
    fireEvent.change(screen.getByLabelText("sets"), {
      target: { value: "3" },
    });
    fireEvent.change(screen.getByLabelText("reps"), {
      target: { value: "10" },
    });
    fireEvent.change(screen.getByLabelText("weight"), {
      target: { value: "100" },
    });
    fireEvent.change(screen.getByLabelText("date"), {
      target: { value: "2023-06-25" },
    });

    // Mock the current date
    const mockToISOString = jest
      .spyOn(Date.prototype, "toISOString")
      .mockReturnValueOnce("2023-06-25T00:00:00.000Z");

    // Mock the localStorage token
    const token = "mockToken";
    localStorage.setItem("token", token);

    await act(async () => {
      // Submit the form
      fireEvent.click(screen.getByText("Log Exercise"));

      // Wait for the state updates to be applied
      await waitFor(() => {
        expect(axios.post).toHaveBeenCalledWith(
          "/api/strength",
          {
            exercise: "Bench Press",
            sets: "3",
            reps: "10",
            weight: "100",
            date: "2023-06-25",
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
      });

      // Check that success toast message is displayed

      expect(toast.success).toHaveBeenCalledWith(
        "Exercise logged successfully"
      );
    });

    mockToISOString.mockRestore();
  });

  test("displays error message when form inputs are missing", () => {
    fireEvent.click(screen.getByText("Log Exercise"));
    expect(toast.error).toHaveBeenCalledWith("Fill in missing inputs!");
  });

  test("displays error message when form inputs are not numbers", () => {
    fireEvent.change(screen.getByLabelText("exercise"), {
      target: { value: "Bench Press" },
    });
    fireEvent.change(screen.getByLabelText("sets"), {
      target: { value: "3" },
    });
    fireEvent.change(screen.getByLabelText("reps"), {
      target: { value: "abc" },
    });
    fireEvent.change(screen.getByLabelText("weight"), {
      target: { value: "100" },
    });
    fireEvent.change(screen.getByLabelText("date"), {
      target: { value: "2023-06-25" },
    });

    fireEvent.click(screen.getByText("Log Exercise"));

    expect(toast.error).toHaveBeenCalledWith(
      "Sets, reps, and weight must be a number!"
    );
  });

  // test("displays today's exercise logs", async () => {
  //   const mockLogs = [
  //     { exercise: "Squats", sets: 3, reps: 8, weight: 200, date: "2023-06-25" },
  //   ];
  //   axios.get.mockResolvedValue({ data: mockLogs });

  //   // await waitFor(() => {
  //   //   expect(axios.get).toHaveBeenCalledWith("/api/strength", {
  //   //     params: { date: expect.any(String) },
  //   //     headers: { Authorization: "Bearer null" }, // Adjust the token value if needed
  //   //   });
  //   // });
  //   await waitFor(() => {
  //     fireEvent.click(screen.getByText("Log Exercise"));
  //   });

  //   expect(screen.getByText("Squats")).toBeInTheDocument();
  //   // expect(screen.getByText("Deadlifts")).toBeInTheDocument();
  // });
});
