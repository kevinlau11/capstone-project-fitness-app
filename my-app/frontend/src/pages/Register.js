import React, { useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./Register.css";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const validateEmail = (email) => {
    // Regular expression pattern for email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const handleRegister = async () => {
    try {
      if (email.trim() === "") {
        toast.error("Email is required");
        return;
      }
      if (password.trim() === "") {
        toast.error("Password is required");
        return;
      }
      if (!validateEmail(email)) {
        toast.error("Invalid email format");
        return;
      }
      const response = await axios.post("/api/register", {
        email,
        password,
      });

      if (response.status === 201) {
        // Registration successful, handle the success case
        toast.success(response.data.message);
        console.log("received 201");
        // console.log(response.data.message);
        setEmail("");
        setPassword("");
      }
    } catch (error) {
      console.error(error.response.data);
      toast.error(error.response.data.message);
    }
  };

  return (
    <div className="mt-4">
      <h2>Register User</h2>
      {/* <form onSubmit={handleRegister}> */}
      <input
        className="input"
        type="email"
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        required
      />
      <input
        className="input"
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        required
      />
      <button className="button" onClick={handleRegister} type="submit">
        Register
      </button>
      {/* </form> */}
    </div>
  );
}

export default Register;
