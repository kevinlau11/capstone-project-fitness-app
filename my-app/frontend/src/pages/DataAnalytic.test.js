// dataAnalytics.test.js
import React from "react";
import { render, screen, act } from "@testing-library/react";
import axios from "axios";
import DataAnalytics from "./DataAnalytics";

jest.mock("axios");

describe("DataAnalytics", () => {
  test("renders data analytics page with table view", async () => {
    // Mock the axios.get() function to return sample data
    axios.get.mockResolvedValue({
      data: {
        dataByDate: {
          "2023-07-21": {
            calories: 100,
            sleep: 8,
            distance: 5,
            duration: 2,
          },
        },
        averageCalories: 120,
        totalCalories: 840,
        averageSleep: 7.5,
        totalSleep: 52.5,
        averageDistance: 4,
        totalDistance: 28,
        averageDuration: 1.5,
        totalDuration: 10.5,
      },
    });

    await act(async () => {
      render(<DataAnalytics />);
    });

    // Check if the headings are rendered
    expect(screen.getByText("Data Analytics")).toBeInTheDocument();
    expect(screen.getByText("Past 7 days")).toBeInTheDocument();

    // Check if the table headers are rendered
    expect(screen.getByText("Date")).toBeInTheDocument();
    expect(screen.getByText("Calories")).toBeInTheDocument();
    expect(screen.getByText("Sleep(hr)")).toBeInTheDocument();
    expect(screen.getByText("Distance(mi)")).toBeInTheDocument();
    expect(screen.getByText("Duration(hr)")).toBeInTheDocument();

    // Check if the data is rendered
    expect(screen.getByText("2023-07-21")).toBeInTheDocument();
    expect(screen.getByText("100")).toBeInTheDocument();
    expect(screen.getByText("8")).toBeInTheDocument();
    expect(screen.getByText("5")).toBeInTheDocument();
    expect(screen.getByText("2")).toBeInTheDocument();
    expect(screen.getByText("Total")).toBeInTheDocument();
    expect(screen.getByText("840")).toBeInTheDocument();
    expect(screen.getByText("52.5")).toBeInTheDocument();
    expect(screen.getByText("28")).toBeInTheDocument();
    expect(screen.getByText("10.5")).toBeInTheDocument();
  });

  test("toggles between table and chart view", async () => {
    axios.get.mockResolvedValue({
      data: {
        dataByDate: {},
        averageCalories: 0,
        totalCalories: 0,
        averageSleep: 0,
        totalSleep: 0,
        averageDistance: 0,
        totalDistance: 0,
        averageDuration: 0,
        totalDuration: 0,
      },
    });

    await act(async () => {
      render(<DataAnalytics />);
    });

    // Initially, the table view should be displayed
    expect(screen.getByText("Date")).toBeInTheDocument();
    expect(screen.queryByTestId("line-chart")).not.toBeInTheDocument();

    // Click the "Toggle View" button
    await act(async () => {
      screen.getByText("Toggle View").click();
    });

    // After clicking, the chart view should be displayed
    expect(screen.getByTestId("line-chart")).toBeInTheDocument();
    expect(screen.queryByText("Date")).not.toBeInTheDocument();

    // Click the "Toggle View" button again
    await act(async () => {
      screen.getByText("Toggle View").click();
    });

    // Should toggle back to the table view
    expect(screen.getByText("Date")).toBeInTheDocument();
    expect(screen.queryByTestId("line-chart")).not.toBeInTheDocument();
  });
});
