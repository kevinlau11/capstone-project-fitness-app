import React from "react";
import {
  render,
  screen,
  fireEvent,
  waitFor,
  act,
} from "@testing-library/react";
import axios from "axios";
import { toast } from "react-toastify";
import ProfileManagement from "./ProfileManagement";

jest.mock("axios");
jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("ProfileManagement", () => {
  it("should fetch and display user profile data", async () => {
    const profileData = {
      age: "22",
      gender: "Male",
      height: "100",
      weight: "150",
    };
    axios.get.mockResolvedValueOnce({ data: profileData });

    const { getByLabelText } = render(<ProfileManagement />);

    await waitFor(() => {
      expect(axios.get).toHaveBeenCalledWith("/api/profile");
      expect(getByLabelText("age").value).toBe(profileData.age);
      expect(getByLabelText("gender").value).toBe(profileData.gender);
      expect(getByLabelText("height").value).toBe(profileData.height);
      expect(getByLabelText("weight").value).toBe(profileData.weight);
    });
  });

  it("should handle profile update and display success toast on successful update", async () => {
    const response = { data: { message: "Profile update successful" } };
    axios.post.mockResolvedValueOnce(response);

    const { getByText } = render(<ProfileManagement />);

    fireEvent.click(getByText("Save Profile"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith(
        "/api/profile",
        expect.any(Object)
      );
      expect(toast.success).toHaveBeenCalledWith("Profile update successful");
    });
  });

  it("should handle profile update and display error toast on failed update", async () => {
    const errorResponse = {
      response: {
        data: { message: "Profile update failed" },
      },
    };
    axios.post.mockRejectedValueOnce(errorResponse);

    const { getByText } = render(<ProfileManagement />);

    fireEvent.click(getByText("Save Profile"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith(
        "/api/profile",
        expect.any(Object)
      );
      expect(toast.error).toHaveBeenCalledWith("Profile update failed");
    });
  });

  it("should handle profile update and display error toast on failed update", async () => {
    const errorResponse = {
      response: {
        data: { message: "Profile update failed" },
      },
    };
    axios.post.mockRejectedValueOnce(errorResponse);

    const { getByText } = render(<ProfileManagement />);

    fireEvent.click(getByText("Save Profile"));

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith(
        "/api/profile",
        expect.any(Object)
      );
      expect(toast.error).toHaveBeenCalledWith("Profile update failed");
    });
  });

  it("should create goal", async () => {
    const response = { data: { message: "Goal saved" } };
    axios.post.mockResolvedValueOnce(response);

    const { getByText } = render(<ProfileManagement />);

    fireEvent.change(screen.getByLabelText("name"), {
      target: { value: "Weight Loss" },
    });
    fireEvent.change(screen.getByLabelText("value"), {
      target: { value: 5 },
    });

    const token = "mockToken";
    localStorage.setItem("token", token);

    await act(async () => {
      // Submit the form
      fireEvent.click(screen.getByText("Save Goal"));

      // Wait for the state updates to be applied
      await waitFor(() => {
        expect(axios.post).toHaveBeenCalledWith(
          "/api/goal",
          {
            name: "Weight Loss",
            value: "5",
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
      });
      expect(toast.success).toHaveBeenCalledWith("Goal saved");
    });
  });
});
