import React from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Toast = () => {
  // Display a success toast
  const handleSuccessToast = () => {
    toast.success("Success message");
  };

  // Display an error toast
  const handleErrorToast = () => {
    toast.error("Error message");
  };

  return (
    <div>
      <button onClick={handleSuccessToast}>Show Success Toast</button>
      <button onClick={handleErrorToast}>Show Error Toast</button>
      <ToastContainer />
    </div>
  );
};

export default Toast;
