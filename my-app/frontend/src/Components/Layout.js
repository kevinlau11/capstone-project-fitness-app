import { Outlet, Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter, Routes, Route, NavLink } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

function Navbars() {
  // console.log(isLoggedIn);
  const navigate = useNavigate();

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  console.log("isLoggedIn");
  const token = localStorage.getItem("token");

  const handleLogout = async () => {
    console.log(isLoggedIn);

    try {
      localStorage.removeItem("token");
      const response = await axios.post("/api/logout");
      toast.success(response.data.message);
      console.log(response.data);
      navigate("/");
    } catch (error) {
      toast.error(error.response.data.message);
      console.error("Logout failed:", error.message);
    }
  };

  if (token) {
    return (
      <Navbar fixed="bottom" bg="light" data-bs-theme="light">
        <Container>
          {/* <Navbar.Brand href="#home">Navbar</Navbar.Brand> */}
          <Nav className="">
            <Nav.Link as={Link} to="/profile">
              Profile
            </Nav.Link>

            <Nav.Link as={Link} to="/strength-log">
              Strength Log
            </Nav.Link>

            <Nav.Link as={Link} to="/cardio-log">
              Cardio Log
            </Nav.Link>
            <Nav.Link as={Link} to="/history">
              History
            </Nav.Link>
            <Nav.Link as={Link} to="/nutrition">
              Nutrition
            </Nav.Link>
            <Nav.Link as={Link} to="/sleep">
              Sleep/Weight
            </Nav.Link>
            <Nav.Link as={Link} to="/exercises">
              Exercises
            </Nav.Link>
            <Nav.Link as={Link} to="/data-analytics">
              Data Analytics
            </Nav.Link>
          </Nav>
          <Nav>
            <Button variant="primary" onClick={handleLogout}>
              Log out
            </Button>
          </Nav>
        </Container>
      </Navbar>
    );
  } else {
    return null;
  }
}

const Layout = () => {
  return (
    <>
      <Navbars />
      <Outlet />
      <ToastContainer />
    </>
  );
};

export default Layout;
