import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import axios from "axios";
import { toast } from "react-toastify";
import Logout from "./Logout";
import { useNavigate } from "react-router-dom";

jest.mock("axios");
jest.mock("react-toastify");
jest.mock("react-router-dom", () => ({
  useNavigate: jest.fn(),
}));

describe("Logout component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("clicking logout button calls the necessary functions and logs out successfully", async () => {
    const successMessage = "Logout successful";
    axios.post.mockResolvedValueOnce({ data: { message: successMessage } });
    toast.success.mockImplementationOnce(() => {});
    const token = "dummyToken";
    localStorage.setItem("token", token);
    const navigate = jest.fn();
    useNavigate.mockReturnValue(navigate);

    render(<Logout />);

    const logoutButton = screen.getByText("Log out");

    fireEvent.click(logoutButton);

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith("/api/logout");
      expect(toast.success).toHaveBeenCalledWith(successMessage);
      expect(navigate).toHaveBeenCalledWith("/");
    });
    expect(localStorage.getItem("token")).toBeNull();
  });
});
