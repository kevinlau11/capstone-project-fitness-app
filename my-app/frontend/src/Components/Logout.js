import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";

const Logout = ({ change }) => {
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      localStorage.removeItem("token");
      const response = await axios.post("/api/logout");
      toast.success(response.data.message);
      console.log(response.data);
      navigate("/");
    } catch (error) {
      toast.error(error.response.data.message);
      console.error("Logout failed:", error.message);
    }
    // change();
  };

  return (
    <Button variant="primary" onClick={handleLogout}>
      Log out
    </Button>
  );
};

export default Logout;
