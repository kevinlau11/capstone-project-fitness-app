import "./App.css";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Register from "./pages/Register";
import Login from "./pages/Login";
import ProfileManagement from "./pages/ProfileManagement";
import {
  BrowserRouter,
  Routes,
  Route,
  Link as RouterLink,
} from "react-router-dom";
import Layout from "./components/Layout";
import CardioLog from "./pages/CardioLog";
import StrengthLog from "./pages/StrengthLog";
import History from "./pages/History";
import Nutrition from "./pages/Nutrition";
import Sleep from "./pages/Sleep";
import Exercises from "./pages/Exercises";
import DataAnalytics from "./pages/DataAnalytics";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    // Handle login logic and set the isLoggedIn state to true upon successful login
    setIsLoggedIn(true);
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout isLoggedIn={isLoggedIn} />}>
          <Route
            component={RouterLink}
            index
            element={
              <div className="page-container">
                <Login onLogin={handleLogin} /> <Register />
              </div>
            }
          />
          <Route
            component={RouterLink}
            path="profile"
            element={<ProfileManagement />}
          />
          <Route
            component={RouterLink}
            path="strength-log"
            element={<StrengthLog />}
          />
          <Route
            component={RouterLink}
            path="cardio-log"
            element={<CardioLog />}
          />
          <Route component={RouterLink} path="history" element={<History />} />
          <Route
            component={RouterLink}
            path="nutrition"
            element={<Nutrition />}
          />
          <Route component={RouterLink} path="sleep" element={<Sleep />} />
          <Route
            component={RouterLink}
            path="exercises"
            element={<Exercises />}
          />
          <Route
            component={RouterLink}
            path="data-analytics"
            element={<DataAnalytics />}
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
