const mongoose = require("mongoose");

const goalSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  name: { type: String, required: true },
  value: { type: Number, required: true },
});

const Goal = mongoose.model("Goal", goalSchema);

module.exports = Goal;
