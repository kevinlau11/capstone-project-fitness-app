const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  length: {
    type: Number,
    required: true,
  },
  weight: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
    unique: true,
  },
});

const Sleep = mongoose.model("Sleep", schema);

module.exports = Sleep;
