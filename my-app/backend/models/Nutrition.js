const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  calories: {
    type: Number,
    required: true,
  },
  carboyhydrates: {
    type: Number,
    required: false,
    default: null,
  },
  fat: {
    type: Number,
    required: false,
    default: null,
  },
  protein: {
    type: Number,
    required: false,
    default: null,
  },
  cholesterol: {
    type: Number,
    required: false,
    default: null,
  },
  sodium: {
    type: Number,
    required: false,
    default: null,
  },
  date: {
    type: Date,
    required: false,
    default: null,
  },
});

const Nutrition = mongoose.model("Nutrition", schema);

module.exports = Nutrition;
