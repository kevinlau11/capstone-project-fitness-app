const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  exercise: {
    type: String,
    required: true,
  },
  sets: {
    type: String,
    required: true,
  },
  reps: {
    type: Number,
    default: null,
  },
  weight: {
    type: Number,
    required: true,
  },
  date: {
    type: Date,
    default: null,
  },
});

const Strength = mongoose.model("Strength", schema);

module.exports = Strength;
