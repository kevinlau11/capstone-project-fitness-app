const express = require("express");
const session = require("express-session");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const cors = require("cors");
const jwt = require("jsonwebtoken");

const User = require("./models/user");
const Cardio = require("./models/Cardio");
const Strength = require("./models/Strength");
const Nutrition = require("./models/Nutrition");
const Sleep = require("./models/Sleep");
const Exercise = require("./models/Exercise");
const Goal = require("./models/Goal");

const app = express();
app.use(express.json());
app.use(cors());
app.use(
  session({
    secret: "your-secret-key",
    resave: false,
    saveUninitialized: false,
  })
);
// Set up the database connection
mongoose
  .connect("mongodb://localhost:27017/fitness-app", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connected to MongoDB"))
  .catch((err) => console.error("Failed to connect to MongoDB:", err));

// Register a new user
app.post("/api/register", async (req, res) => {
  try {
    const { email, password } = req.body;

    // Check if user already exists with the given email
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res
        .status(401)
        .json({ message: "User with this email already exists" });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user with hashed password
    const user = new User({ email, password: hashedPassword });
    await user.save();

    res.status(201).json({ message: "Registration successful" });
  } catch (error) {
    res.status(500).json({ message: "Failed to register user" });
  }
});

// Login with existing user
app.post("/api/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });

    // Check if user exists with the given email
    if (!user) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    // Compare the provided password with the stored hashed password
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ message: "Invalid email or password" });
    }
    req.session.user = { email };
    const token = jwt.sign({ userId: user._id }, "secret-key");
    res.status(200).json({ message: "Logged in successfully", token: token });
  } catch (error) {
    res.status(500).json({ message: "Failed to login" });
  }
});

app.post("/api/logout", (req, res) => {
  // Clear the user session
  req.session.destroy((error) => {
    if (error) {
      res.status(500).json({ error: "Failed to logout." });
    } else {
      res.status(200).json({ message: "Logged out successfully" });
    }
  });
});

// Define user profile management endpoints
app.get("/api/profile", async (req, res) => {
  try {
    // Retrieve the logged-in user's email from the session
    const { email } = req.session.user;

    // Find the user by email
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Return the user's profile data
    res.status(200).json({
      age: user.age,
      gender: user.gender,
      height: user.height,
      weight: user.weight,
    });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

app.post("/api/profile", async (req, res) => {
  try {
    const { age, gender, height, weight } = req.body;

    const { email } = req.session.user;
    console.log({ email });
    // Find the user by userId
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Update user profile data
    user.age = age;
    user.gender = gender;
    user.height = height;
    user.weight = weight;

    // Save the updated user
    await user.save();

    res.status(200).json({ message: "Profile saved successfully" });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

// Get workouts for a specific user
app.get("/api/cardio", authenticateToken, async (req, res) => {
  try {
    const { date } = req.query;
    const userId = req.user.userId;
    const cardio = await Cardio.find({ date: date, userId: userId });
    res.json(cardio);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get cardio exercises" });
  }
});

app.post("/api/cardio", authenticateToken, async (req, res) => {
  try {
    const { exercise, distance, duration, date } = req.body;
    const userId = req.user.userId; // Extracted from the token

    // Create a new cardio exercise
    const cardio = new Cardio({ userId, exercise, distance, duration, date });
    await cardio.save();

    res.status(201).json({ message: "Log successful" });
  } catch (error) {
    res
      .status(500)
      .json({ message: "Failed to log cardio exercise: " + error });
  }
});

// Get workouts for a specific user
app.get("/api/strength", authenticateToken, async (req, res) => {
  try {
    const { date } = req.query;
    const userId = req.user.userId;
    const strength = await Strength.find({ date: date, userId: userId });
    res.json(strength);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get exercise" });
  }
});

app.post("/api/strength", authenticateToken, async (req, res) => {
  try {
    const { exercise, sets, reps, weight, date } = req.body;
    const userId = req.user.userId; // Extracted from the token

    // Create a new exercise
    const strength = new Strength({
      userId,
      exercise,
      sets,
      reps,
      weight,
      date,
    });
    await strength.save();

    res.status(201).json({ message: "Log successful" });
  } catch (error) {
    res.status(500).json({ message: "Failed to log exercise: " + error });
  }
});

app.get("/api/nutrition", authenticateToken, async (req, res) => {
  try {
    const { date } = req.query;
    const userId = req.user.userId;
    const nutrition = await Nutrition.find({ date: date, userId: userId });
    res.json(nutrition);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get nutrition" });
  }
});

app.post("/api/nutrition", authenticateToken, async (req, res) => {
  try {
    const {
      calories,
      carbohydrates,
      fat,
      protein,
      sugar,
      cholesterol,
      sodium,
      date,
    } = req.body;
    const userId = req.user.userId; // Extracted from the token

    app.get("/api/nutrition", authenticateToken, async (req, res) => {
      try {
        const { date } = req.query;
        const userId = req.user.userId;
        const nutrition = await Nutrition.find({ date: date, userId: userId });
        res.json(nutrition);
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Failed to get nutrition" });
      }
    });

    const nutrition = new Nutrition({
      userId,
      calories,
      carbohydrates,
      fat,
      protein,
      sugar,
      cholesterol,
      sodium,
      date,
    });
    await nutrition.save();

    res.status(201).json({ message: "Log successful" });
  } catch (error) {
    res.status(500).json({ message: "Failed to log nutrition: " + error });
  }
});

app.get("/api/sleep", authenticateToken, async (req, res) => {
  try {
    const { date } = req.query;
    const userId = req.user.userId;
    const sleep = await Sleep.find({ date: date, userId: userId });
    res.json(sleep);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get sleep" });
  }
});

app.post("/api/sleep", authenticateToken, async (req, res) => {
  try {
    const { length, weight, date } = req.body;
    const userId = req.user.userId; // Extracted from the token

    const sleep = new Sleep({
      userId,
      length,
      weight,
      date,
    });
    await sleep.save();

    res.status(201).json({ message: "Log successful" });
  } catch (error) {
    res.status(500).json({ message: "Failed to log sleep: " + error });
  }
});

app.get("/api/goal", authenticateToken, async (req, res) => {
  try {
    const userId = req.user.userId;
    const goal = await Goal.find({ userId: userId });
    res.json(goal);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get goal" });
  }
});

app.post("/api/goal", authenticateToken, async (req, res) => {
  try {
    const { name, value } = req.body;
    const userId = req.user.userId; // Extracted from the token

    const goal = await Goal.findOne({ userId });

    if (!goal) {
      const newGoal = new Goal({
        userId,
        name,
        value,
      });
      await newGoal.save();
    } else {
      goal.name = name;
      goal.value = value;
      await goal.save();
    }

    res.status(201).json({ message: "Goal Saved" });
  } catch (error) {
    console.log("");
    res.status(500).json({ message: "Failed to save goal: " + error });
  }
});

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) {
    return res.status(401).json({ message: "Authentication token missing" });
  }

  jwt.verify(token, "secret-key", (err, user) => {
    if (err) {
      return res.status(403).json({ message: "Invalid token" });
    }

    req.user = user;
    next();
  });
}

app.get("/api/exercise", authenticateToken, async (req, res) => {
  try {
    const exercise = await Exercise.find();
    res.json(exercise);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to get exercisees" });
  }
});

const exercisesData = [
  {
    name: "Push-up",
    description:
      "Push-ups are a great bodyweight exercise that targets the chest, shoulders, and triceps.",
  },
  {
    name: "Squats",
    description:
      "Squats are a fundamental lower body exercise that targets the quadriceps, hamstrings, and glutes.",
  },
  {
    name: "Pull-up",
    description:
      "Pull-ups are an excellent upper body exercise that targets the back, biceps, and shoulders.",
  },
  {
    name: "Lunges",
    description:
      "Lunges are a unilateral lower body exercise that targets the quads, hamstrings, and glutes.",
  },
  {
    name: "Deadlift",
    description:
      "Deadlifts are a compound exercise that targets the lower back, glutes, and hamstrings.",
  },
  {
    name: "Bench Press",
    description:
      "Bench Press is a popular strength exercise that targets the chest, shoulders, and triceps.",
  },
  {
    name: "Plank",
    description:
      "The Plank is a core-strengthening exercise that targets the abdominals and stabilizing muscles.",
  },
  {
    name: "Bicep Curl",
    description:
      "Bicep Curls target the biceps and are a common isolation exercise for arm strength.",
  },
  {
    name: "Crunches",
    description:
      "Crunches are a classic abdominal exercise that targets the rectus abdominis (six-pack muscles).",
  },
  {
    name: "Tricep Dips",
    description:
      "Tricep Dips are a bodyweight exercise that targets the triceps and shoulders.",
  },
  {
    name: "Leg Press",
    description:
      "Leg Press is a machine-based exercise that targets the quadriceps, hamstrings, and glutes.",
  },
  // Add more exercises here
];

app.get("/api/calcdata", authenticateToken, async (req, res) => {
  try {
    const { email } = req.session.user;
    const userId = req.user.userId;

    // Find the user by email
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Find nutrition data for the last 7 days
    const lastWeek = new Date();
    lastWeek.setDate(lastWeek.getDate() - 7);
    const nutritionData = await Nutrition.find({
      date: { $gte: lastWeek, $lte: new Date() },
      userId: userId,
    });

    // Find sleep data for the last 7 days
    const sleepData = await Sleep.find({
      date: { $gte: lastWeek, $lte: new Date() },
      userId: userId,
    });

    // Find cardio data for the last 7 days
    const cardioData = await Cardio.find({
      date: { $gte: lastWeek, $lte: new Date() },
      userId: userId,
    });

    // Create an object to store data for each date
    const dataByDate = {};

    // Calculate total calories and sleep for each date
    nutritionData.forEach((entry) => {
      const date = entry.date.toISOString().split("T")[0]; // Get the date in "YYYY-MM-DD" format
      if (!dataByDate[date]) {
        dataByDate[date] = {
          calories: null,
          sleep: null,
          distance: null,
          duration: null,
        };
      }
      dataByDate[date].calories += entry.calories;
    });

    sleepData.forEach((entry) => {
      const date = entry.date.toISOString().split("T")[0]; // Get the date in "YYYY-MM-DD" format
      if (!dataByDate[date]) {
        dataByDate[date] = {
          calories: null,
          sleep: null,
          distance: null,
          duration: null,
        };
      }
      dataByDate[date].sleep += entry.length;
    });

    cardioData.forEach((entry) => {
      const date = entry.date.toISOString().split("T")[0]; // Get the date in "YYYY-MM-DD" format
      if (!dataByDate[date]) {
        dataByDate[date] = {
          calories: null,
          sleep: null,
          distance: null,
          duration: null,
        };
      }
      if (dataByDate[date].distance === null) {
        dataByDate[date].distance = null;
      }
      if (dataByDate[date].duration === null) {
        dataByDate[date].duration = null;
      }
      dataByDate[date].distance += entry.distance;
      dataByDate[date].duration += entry.duration;
    });

    // Calculate average and total calories for the last 7 days
    const totalCalories = nutritionData.reduce(
      (total, entry) => total + entry.calories,
      0
    );
    const averageCalories =
      nutritionData.length === 0 ? 0 : totalCalories / nutritionData.length;
    // Calculate average and total sleep for the last 7 days
    const totalSleep = sleepData.reduce(
      (total, entry) => total + entry.length,
      0
    );
    const averageSleep =
      sleepData.length === 0 ? 0 : totalSleep / sleepData.length;

    // Calculate average and total distance for the last 7 days
    const totalDistance = cardioData.reduce(
      (total, entry) => total + entry.distance,
      0
    );
    const averageDistance =
      cardioData.length === 0 ? 0 : totalDistance / cardioData.length;

    // Calculate average and total duration for the last 7 days
    const totalDuration = cardioData.reduce(
      (total, entry) => total + entry.duration,
      0
    );
    const averageDuration =
      cardioData.length === 0 ? 0 : totalDuration / cardioData.length;
    // Return the user's profile data along with the calculated values
    res.status(200).json({
      age: user.age,
      averageCalories: averageCalories,
      totalCalories: totalCalories,
      averageSleep: averageSleep,
      totalSleep: totalSleep,
      averageDistance: averageDistance,
      totalDistance: totalDistance,
      averageDuration: averageDuration,
      totalDuration: totalDuration,
      dataByDate: dataByDate,
    });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

async function seedExercises() {
  try {
    await Exercise.deleteMany({}); // Clear existing exercises
    const createdExercises = await Exercise.insertMany(exercisesData);
  } catch (error) {
    console.error("Error seeding exercises:", error);
  }
}

seedExercises();

// Start the server
const port = 3001;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
